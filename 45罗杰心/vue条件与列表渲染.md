## v-if
```html
<h1 v-if="awesome">Vue is awesome!</h1>
```

## v-else

你也可以使用 v-else 为 v-if 添加一个“else 区块”。

```html
<button @click="awesome = !awesome">Toggle</button>

<h1 v-if="awesome">Vue is awesome!</h1>
<h1 v-else>Oh no 😢</h1>
```
## v-else-if​
顾名思义，v-else-if 提供的是相应于 v-if 的“else if 区块”。它可以连续多次重复使用
和 v-else 类似，一个使用 v-else-if 的元素必须紧跟在一个 v-if 或一个 v-else-if 元素后面。

## v-show
另一个可以用来按条件显示一个元素的指令是 v-show。其用法基本一样：
```html
<h1 v-show="ok">Hello!</h1>
```
v-show 不支持在 <template> 元素上使用，也不能和 v-else 搭配使用。

## v-for

我们可以使用 v-for 指令基于一个数组来渲染一个列表。v-for 指令的值需要使用 item in items 形式的特殊语法，其中 items 是源数据的数组，而 item 是迭代项的别名：
```js
data() {
  return {
    items: [{ message: 'Foo' }, { message: 'Bar' }]
  }
}
```
```html
<li v-for="item in items">
  {{ item.message }}
</li>
```

在 v-for 块中可以完整地访问父作用域内的属性和变量。v-for 也支持使用可选的第二个参数表示当前项的位置索引。
```js
data() {
  return {
    parentMessage: 'Parent',
    items: [{ message: 'Foo' }, { message: 'Bar' }]
  }
}
```
```html
<li v-for="(item, index) in items">
  {{ parentMessage }} - {{ index }} - {{ item.message }}
</li>
```

## 在 v-for 里使用范围值​
v-for 可以直接接受一个整数值。在这种用例中，会将该模板基于 1...n 的取值范围重复多次。
```html
<span v-for="n in 10">{{ n }}</span>
```
注意此处 n 的初值是从 1 开始而非 0。



## v-for与v-if
同时使用 v-if 和 v-for 是不推荐的，因为这样二者的优先级不明显
当 v-if 和 v-for 同时存在于一个元素上的时候，v-if 会首先被执行。
当它们同时存在于一个节点上时，v-if 比 v-for 的优先级更高。这意味着 v-if 的条件将无法访问到 v-for 作用域内定义的变量别名：

```html
<!--
 这会抛出一个错误，因为属性 todo 此时
 没有在该实例上定义
-->
<li v-for="todo in todos" v-if="!todo.isComplete">
  {{ todo.name }}
</li>
```
在外新包装一层 <template> 再在其上使用 v-for 可以解决这个问题 (这也更加明显易读)：

```html
<template v-for="todo in todos">
  <li v-if="!todo.isComplete">
    {{ todo.name }}
  </li>
</template>
```
## 通过 key 管理状态

为了给 Vue 一个提示，以便它可以跟踪每个节点的标识，从而重用和重新排序现有的元素，你需要为每个元素对应的块提供一个唯一的 key attribute：

```html
<div v-for="item in items" :key="item.id">
  <!-- 内容 -->
</div>
```
当你使用 <template v-for> 时，key 应该被放置在这个 <template> 容器上：

```html
<template v-for="todo in todos" :key="todo.name">
  <li>{{ todo.name }}</li>
</template>

```


## 变更方法​
Vue 能够侦听响应式数组的变更方法，并在它们被调用时触发相关的更新。这些变更方法包括：

push()
pop()
shift()
unshift()
splice()
sort()
reverse()