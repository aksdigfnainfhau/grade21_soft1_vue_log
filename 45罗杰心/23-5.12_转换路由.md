# 排除一下主页面的路由，使其不会出现在菜单栏

## 步骤

+ 1、将导入的路由数据转换一下，以使下级path带上上级path，以便组成完成的路由路径(请看上个笔记)

+ 2.排除一下主页面的路由，使其不会出现在菜单栏

    + a.  将导入的路由中的主页从菜单栏数据排除

    + b. 定义一个空数组,用于存储处理好的路由

    + c. 循环遍历传进的数组，处理每一个路由

        + (1). 定义一个对象，存储需要的数据

        + (2). 特殊处理，指当前菜单项标识隐藏的时候，则将其下级菜单项的第一个，提取为显示的菜单

        + (3). 判断当path为空时,进行相关的操作

        + (4). 将处理好的路由插入到空的数组中
    
    + d. 返回定义的存储变量


+ 3.返回处理好的菜单栏数据

```js
// 使用计算属性，将导入的路由，转换成菜单栏数据
const menus = computed(() => {
    //1、将导入的路由数据转换一下，以使下级path带上上级path，以便组成完成的路由路径
    let list = converPath(routes, '')

    // 2.排除一下主页面的路由，使其不会出现在菜单栏
    let menuList=processHidden(list)

    // 3.返回处理好的菜单栏数据
    return menuList
})
```

#
## 代码
```js
// 将导入的路由中的主页从菜单栏数据排除
// 暴露方法
export function processHidden(arr){
    let list=[]

    arr.forEach(item=>{
        let obj=''

        // 特殊处理，指当前菜单项标识隐藏的时候，则将其下级菜单项的第一个，提取为显示的菜单
        if(item.men && item.men.hidden && item.children && item.children.length){
            // 获取下级的第一个数据
            let children=item.children[0]
            // 将数据输出
            obj=children
        }else{
            // 
            obj=item
        }

        // 判断当path为空时
        if(!obj.path){
            obj.path='/'
        }

        // 将处理后的路由赋值
        list.push(obj)
    })

    return list
}
```